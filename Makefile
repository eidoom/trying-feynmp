.PHONY: all clean

all: partial.pdf

partial.mp: main.tex
	latex $<

partial.1: partial.mp
	mpost $(basename $<) $@

partial.pdf: partial.1
	epstopdf $< $@

clean:
	-rm *.log *.mp *.pdf *.aux *.dvi *.1 *.t1
